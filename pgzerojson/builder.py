from functools import partial
from itertools import chain
from typing import \
    TYPE_CHECKING, Any, Callable, Dict, Iterable, Iterator, List, Optional, Sequence, Set, Tuple, \
    Type, Union
import uuid

import pydantic
from pydantic.fields import FieldInfo
import sqlalchemy
from sqlalchemy import Column, MetaData, Table, column
from sqlalchemy.engine import Engine
from sqlalchemy.ext import compiler
from sqlalchemy.sql.expression import (
    Alias,
    CTE,
    ColumnElement,
    FromClause,
    SelectBase,
    Selectable,
    func,
    text,
)
TableCTE = Union[CTE, Table]


if TYPE_CHECKING:
    from pydantic.main import Model


def qualified_str(table: str, column: str) -> str:
    return (f'"{table}"."{column}"' if table
            else f'"{column}"')


def qualified(table: Table, field: Column) -> str:
    return qualified_str(table.name, field.name)


def drop_id(s: str) -> str:
    return str(s[:-3] if s.endswith('_id') else s)


def plural(s: str) -> str:
    return s + "s"


def fix_pydantic_field_name(name: str, field_info: dict) -> str:
    try:
        pydantic.utils.validate_field_name([pydantic.BaseModel], name)
    except NameError:
        name, field_info['alias'] = (name + '_', name)
    return name


def make_pydantic_field_info(name: str, type_: type) -> Tuple[str, dict]:
    field_info = {}
    if isinstance(type_, sqlalchemy.types.String):
        field_info['maxLength'] = type_.length
    name = fix_pydantic_field_name(name, field_info)
    return name, field_info


def get_referenced_by(table: TableCTE,
                      metadata: sqlalchemy.MetaData,
                      skip_tables: Set[TableCTE]) -> Iterator[Tuple[Table, Column, Column]]:
    found = False
    for t in metadata.sorted_tables:
        if t == table:
            found = True
        if not found or t in skip_tables:
            continue
        for fk in t.foreign_key_constraints:
            local_column = list(fk.columns)[0]
            other_column = list(local_column.foreign_keys)[0].column
            if other_column.table == table:
                yield t, local_column, other_column


def get_python_type(sqltype: sqlalchemy.sql.type_api.TypeEngine) -> type:
    try:
        return sqltype.python_type
    except NotImplementedError:
        if isinstance(sqltype, sqlalchemy.dialects.postgresql.base.UUID):
            return uuid.UUID
        raise


AddJoinTarget = Union[
    Tuple[FromClause, str, str],
    Tuple[FromClause, str, Callable[[FromClause, FromClause], ColumnElement]],
]


class PgZeroJsonConfig(pydantic.BaseModel):
    table: TableCTE
    alias_name: Optional[str]
    metadata: MetaData
    add_fields: Dict[TableCTE, Sequence[Tuple[str,
                                              ColumnElement,
                                              Tuple[type, FieldInfo]]]] = {}
    add_joins: Dict[TableCTE, Sequence[AddJoinTarget]] = {}
    add_inner_joins: Dict[TableCTE, Sequence[Tuple[FromClause, str, str]]] = {}
    allow_cte_materialization_tables: Set[TableCTE] = set()
    aux_schema: Optional[str] = None
    leaf_tables: Set[TableCTE] = set()
    many_to_many_relationships: Dict[TableCTE, Dict[str, Tuple[str, Table, str,
                                                               str, Table, str]]] = {}
    materialized_view_tables: Set[TableCTE] = set()
    order_keys: Sequence[ColumnElement] = ()
    rename_fields: Dict[TableCTE, Dict[str, str]] = {}
    rename_relationships: Dict[TableCTE, Dict[str, str]] = {}
    skip_fields: Dict[Optional[TableCTE], Set[str]] = {}
    skip_tables: Set[TableCTE] = set()
    subquery_tables: Set[TableCTE] = set()
    title_getter: Callable[[str, str, str], str]

    def get_title(self, name: str, comment: str, table: str) -> str:
        return self.__dict__['title_getter'](name, comment, table)

    def replace(self, **kwargs) -> 'PgZeroJsonConfig':
        return PgZeroJsonConfig(**dict(self.dict(), **kwargs))

    class Config:
        arbitrary_types_allowed = True


class PgZeroJsonState(pydantic.BaseModel):
    cte: Dict[str, FromClause]
    do_pydantic: bool = False
    pydantic_models: Dict[str, type]
    path: Tuple[str, ...] = ()
    where: Dict[Tuple[str, ...], List[ColumnElement]] = {}

    class Config:
        arbitrary_types_allowed = True


def get_table_alias_name(table: TableCTE, cfg: PgZeroJsonConfig) -> str:
    if table is cfg.table and cfg.alias_name:
        return cfg.alias_name
    assert table.name
    return table.name


def get_real_table(table: TableCTE) -> TableCTE:
    if isinstance(table, Alias) and not isinstance(table, CTE):
        return table.element
    return table


def get_select(table: TableCTE,
               cfg: PgZeroJsonConfig,
               state: PgZeroJsonState,
               skip_tables: Set[TableCTE],
               only_complex_or_renamed: bool = False):
    table_name = get_table_alias_name(table, cfg)
    assert table_name
    path = state.path
    subquery = partial(select_json_sql,
                       cfg=cfg,
                       state=state,
                       skip_tables=skip_tables)
    _ensure_cte = partial(ensure_cte, cfg=cfg, state=state,
                          skip_tables=skip_tables)
    real_table = get_real_table(table)
    table_skip_fields = set(chain(cfg.skip_fields.get(None, ()),
                                  cfg.skip_fields.get(real_table, ())))
    joins = []
    pydantic_fields: Dict[str, Tuple[type, FieldInfo]] = {}

    for join_target in cfg.add_joins.get(real_table, ()):
        if isinstance(join_target[2], str):
            other_table, this_column_name, other_column_name = join_target
            subalias = other_table.alias(f"aj_{this_column_name}")
            joins.append((subalias, (table.c[this_column_name] == subalias.c[other_column_name])))
        elif callable(join_target[2]):
            other_table, alias, expr_fun = join_target
            subalias = other_table.alias(f"aj_{alias}")
            joins.append((subalias, expr_fun(table, subalias)))
        else:
            raise ValueError(join_target)

    for f in (_ for _ in table.columns if _.name not in table_skip_fields):
        if not f.foreign_keys:
            # Scalar field
            renamed_name = str(cfg.rename_fields.get(real_table, {}).get(f.name, f.name))
            if only_complex_or_renamed and renamed_name == f.name:
                continue
            if state.do_pydantic:
                py_type = get_python_type(f.type)
                if py_type is list:
                    py_type = List[get_python_type(f.type.item_type)]  # type: ignore
                renamed_name, field_info = make_pydantic_field_info(renamed_name, f.type)
                validators = getattr(f, 'info', {})
                nullable = getattr(f, 'nullable', False)
                comment = getattr(f, 'comment', None)
                if nullable:
                    py_type = Optional[py_type]
                pydantic_fields[renamed_name] = (
                    py_type,
                    pydantic.Field(None if nullable else ...,
                                   title=cfg.get_title(renamed_name, comment, table_name),
                                   description=comment,
                                   **validators,
                                   **field_info),
                )
            yield renamed_name, f
        else:
            # ForeignKey
            r = next(iter(f.foreign_keys)).column
            if r.table not in skip_tables:
                name = drop_id(f.name)
                renamed_name = cfg.rename_fields.get(real_table, {}).get(name, name)
                state.path = path + (renamed_name,)
                if r.table in cfg.subquery_tables and not state.do_pydantic:
                    r_alias = r.table.alias()
                    jbo, add_joins = make_jbo(r_alias, cfg, state, skip_tables)
                    jbo_q = sqlalchemy.sql.select([jbo])
                    if add_joins:
                        jbo_q = query_add_joins(jbo_q, r_alias, add_joins)
                    jbo_q = jbo_q.where(f == r_alias.c[r.name])
                    yield renamed_name, jbo_q.as_scalar()
                else:
                    jcte = _ensure_cte(table=r.table)
                    subalias = jcte.alias()
                    joins.append((subalias, (f == subalias.c[list(r.table.primary_key)[0].name])))
                    if state.do_pydantic:
                        renamed_name, field_info = make_pydantic_field_info(renamed_name, dict)
                        pydantic_fields[renamed_name] = (
                            state.pydantic_models[get_table_alias_name(r.table, cfg)],
                            pydantic.Field(None if f.nullable else ...,
                                           title=cfg.get_title(renamed_name, f.comment, table_name),
                                           description=f.comment,
                                           **field_info),
                        )
                    yield renamed_name, subalias.c.data
                state.path = path

    # Added fields
    for name, expr, pydantic_field in cfg.add_fields.get(real_table, ()):
        renamed_name = cfg.rename_fields.get(real_table, {}).get(name, name)
        if state.do_pydantic:
            pydantic_fields[renamed_name] = pydantic_field
        yield renamed_name, expr

    # Many-to-many relationships
    for name, (title, thru_table, left_fk, right_fk, target_table, target_pk) in \
            cfg.many_to_many_relationships.get(real_table, {}).items():
        if target_table in skip_tables:
            continue
        state.path = path + (name,)
        jcte = _ensure_cte(table=target_table)
        if state.do_pydantic:
            renamed_name, field_info = make_pydantic_field_info(name, dict)
            jmodel = state.pydantic_models[get_table_alias_name(target_table, cfg)]
            pydantic_fields[name] = (
                List[jmodel],  # type: ignore
                pydantic.Field(None,
                               title=cfg.get_title(name, title, table_name),
                               **field_info),
            )
        this_id = list(table.primary_key)[0]
        yield (
            name,  # renamed later
            subquery(
                table=target_table,
                many=True,
                where={state.path: [(this_id == thru_table.c[left_fk]),
                                    (thru_table.c[right_fk] == jcte.c[target_pk])]}).as_scalar()
        )
        state.path = path

    if real_table not in cfg.leaf_tables:
        # One-to-many relationships
        for t, fk_column, this_id_column in get_referenced_by(
            real_table,
            cfg.metadata,
            skip_tables | set(
                thru_table for _, thru_table, *_ in chain.from_iterable(
                    m.values() for m in cfg.many_to_many_relationships.values()
                )
            ),
        ):
            t_name = get_table_alias_name(t, cfg)
            name_s = plural(t_name)
            renamed_name = cfg.rename_relationships.get(real_table, {}).get(name_s, name_s)
            if renamed_name == name_s:
                print(f"at {table_name}.{renamed_name}:"
                      f" entering {t_name}")
            state.path = path + (renamed_name,)
            jcte = _ensure_cte(table=t, parent_fk=fk_column)
            if state.do_pydantic:
                renamed_name, field_info = make_pydantic_field_info(renamed_name, dict)
                jmodel = state.pydantic_models[t_name]
                pydantic_fields[renamed_name] = (
                    List[jmodel],  # type: ignore
                    pydantic.Field(None,
                                   title=cfg.get_title(renamed_name, t.comment, table_name),
                                   **field_info),
                )
            yield (renamed_name,  # renamed later
                   subquery(table=t,
                            many=True,
                            where={state.path: [this_id_column == jcte.c.pid]}).as_scalar())
            state.path = path
    if state.do_pydantic:
        pydantic_model_name = ((cfg.alias_name if table is cfg.table else None) or
                               (getattr(table, 'info', {}) or {}).get("title") or
                               str(table_name))

        class Config:
            @staticmethod
            def schema_extra(schema: Dict[str, Any], model: Type['Model']) -> None:
                # TODO: try removing with pydantic#1438 release
                for prop in schema.get('properties', {}).values():
                    if "allOf" in prop and len(prop["allOf"]) == 1 and "$ref" in prop["allOf"][0]:
                        prop.update(prop["allOf"][0])
                        del prop["allOf"]

        pydantic_model = pydantic.create_model(pydantic_model_name,
                                               __base__=None,
                                               __config__=Config,  # type: ignore
                                               __module__=None,
                                               __validators__=None,
                                               **pydantic_fields)
        state.pydantic_models[table_name] = pydantic_model
    yield joins


def json_object_sql(
    table: TableCTE,
    skip_tables: Set[TableCTE],
    cfg: PgZeroJsonConfig,
    state: PgZeroJsonState,
    only_complex_or_renamed: bool = False,
) -> Tuple[List[str],
           List[Tuple[ColumnElement, ColumnElement]],
           List[Tuple[sqlalchemy.sql.Alias, ColumnElement]]]:
    skip_tables = skip_tables | {table}
    select = get_select(table,
                        skip_tables=skip_tables, cfg=cfg, state=state,
                        only_complex_or_renamed=only_complex_or_renamed)
    *jbo_pairs, joins = ((t if isinstance(t, tuple) else t) for t in select)
    if only_complex_or_renamed:
        real_table = get_real_table(table)
        skiplist = \
            filter((lambda _: hasattr(table.c, _)),
                   chain(cfg.skip_fields.get(None, ()),
                         cfg.skip_fields.get(real_table, ()),
                         cfg.rename_fields.get(real_table, {}).keys(),
                         (_.name for fk in getattr(table, 'foreign_key_constraints', ())
                          for _ in fk.columns)))
    else:
        skiplist = iter(())
    jbo_args = list(chain.from_iterable((text(f"'{name}'"), expr)
                                        for name, expr in jbo_pairs))
    return list(skiplist), jbo_args, joins


class CreateMaterializedView(sqlalchemy.schema.DDLElement):
    def __init__(self, name: str, selectable: Selectable, schema: Optional[str]):
        self.name = name
        self.selectable = selectable
        self.schema = schema


@compiler.compiles(CreateMaterializedView)
def compile(element: CreateMaterializedView, compiler, **kw) -> str:
    return 'CREATE MATERIALIZED VIEW %s AS %s' % (
        f'"{element.schema}"."{element.name}"' if element.schema else f'"{element.name}"',
        compiler.sql_compiler.process(element.selectable, literal_binds=True),
    )


def make_jbo(
    table: TableCTE,
    cfg: PgZeroJsonConfig,
    state: PgZeroJsonState,
    skip_tables: Set[TableCTE],
) -> Tuple[ColumnElement,
           Sequence[Tuple[sqlalchemy.sql.Alias, ColumnElement]]]:
    skiplist, jbo_args, add_joins = \
        json_object_sql(table,
                        cfg=cfg,
                        state=state,
                        skip_tables=skip_tables)
    if len(jbo_args) > 50 and not state.do_pydantic:
        # Can't pass >100 arguments to a function.
        # Use slower but more compact expression:
        # to_jsonb(table) - 'skipfield1' ... - 'skipfieldN' || '{overriding json}'
        skiplist, jbo_args, add_joins = \
            json_object_sql(table,
                            cfg=cfg,
                            state=state,
                            skip_tables=skip_tables,
                            only_complex_or_renamed=True)
        simple_data = func.to_jsonb(text(f'"{table.name}"'))
        for name in skiplist:
            simple_data = simple_data - text(f"'{name}'")
        if jbo_args:
            simple_data = simple_data.concat(func.jsonb_build_object(*jbo_args))
    else:
        simple_data = func.jsonb_build_object(*jbo_args)
    return func.jsonb_strip_nulls(simple_data), add_joins


def query_add_joins(
        q: sqlalchemy.sql.expression.Select,
        table: TableCTE,
        joins: Iterable[Tuple[sqlalchemy.sql.Alias, ColumnElement]],
        inner_joins: Iterable[Tuple[sqlalchemy.sql.Alias, ColumnElement]] = [],
) -> sqlalchemy.sql.expression.Select:
    join: FromClause = table
    for right, onclause in inner_joins:
        join = join.join(right, onclause)
    for right, onclause in joins:
        join = join.outerjoin(right, onclause)
    return q.select_from(join)


def ensure_cte(table: TableCTE,
               cfg: PgZeroJsonConfig,
               skip_tables: Set[TableCTE],
               state: PgZeroJsonState,
               parent_fk=None, order_keys=(),
               outerjoins: Sequence[Tuple[sqlalchemy.sql.Alias, ColumnElement]] = []) -> FromClause:
    subalias = f'j_{get_table_alias_name(table, cfg)}'
    if subalias not in state.cte:
        real_table = get_real_table(table)
        if (not state.do_pydantic) and real_table in cfg.materialized_view_tables and \
                f'{cfg.aux_schema}.{subalias}' in cfg.metadata.tables:
            state.cte[subalias] = cfg.metadata.tables[f'{cfg.aux_schema}.{subalias}']
            return state.cte[subalias]

        if table.primary_key:
            c_id = list(table.primary_key)[0]
        c_pid = None if parent_fk is None else parent_fk.label('pid')
        simple_data, add_joins = make_jbo(table, cfg, state, skip_tables)
        c_data = simple_data.label('data')

        cols = [c_id] if table.primary_key else []
        if c_pid is not None:
            cols.append(c_pid)
        if order_keys:
            cols.extend(order_keys)
        cols.append(c_data)
        q = sqlalchemy.sql.select(cols)
        if outerjoins or add_joins or real_table in cfg.add_inner_joins:
            q = query_add_joins(q, table, chain(outerjoins, add_joins),
                                [((alias := t.alias(f'aj_{l}')), (column(l) == alias.c[r]))
                                 for t, l, r in cfg.add_inner_joins.get(real_table, [])])
        for clause in state.where.get(state.path, []):
            q = q.where(clause)

        if real_table in cfg.materialized_view_tables and isinstance(cfg.metadata.bind, Engine):
            cfg.metadata.bind.execute(CreateMaterializedView(subalias, q, cfg.aux_schema))
            t = Table(subalias, cfg.metadata, autoload=True, schema=cfg.aux_schema)
            sqlalchemy.Index(f'ix_{subalias}_id', t.c[c_id.name]).create()  # type: ignore  # FIXME
            if c_pid is not None:
                sqlalchemy.Index(f'ix_{subalias}_pid', t.c.pid).create()
            for order_key in order_keys:
                sqlalchemy.Index(f'ix_{subalias}_{order_key.name}', t.c[order_key.name]).create()
            state.cte[subalias] = t
            return state.cte[subalias]

        jcte = q.cte(name=subalias)
        if real_table not in cfg.allow_cte_materialization_tables:
            jcte = jcte.prefix_with('NOT MATERIALIZED')
        state.cte[subalias] = jcte
    return state.cte[subalias]


def select_json_sql(table: TableCTE,
                    cfg: PgZeroJsonConfig,
                    skip_tables: Set[TableCTE] = set(),
                    order_keys: Sequence[ColumnElement] = (),
                    many: bool = False,
                    where: Dict[Tuple[str, ...], List[ColumnElement]] = {},
                    state: PgZeroJsonState = None) -> SelectBase:
    """Generate SQL to fetch table data as JSON"""

    top_call = state is None
    if state is None:
        state = PgZeroJsonState(cte={}, do_pydantic=False, pydantic_models={}, where=where)
        skip_tables = cfg.skip_tables
    else:
        t_where = state.where.copy()
        for t, clauses in where.items():
            state.where[t] = state.where.get(t, []) + clauses

    skip_tables = skip_tables | {table}
    jcte = ensure_cte(table=table,
                      cfg=cfg,
                      order_keys=order_keys,
                      state=state,
                      skip_tables=skip_tables)
    col = jcte.c.data
    q = sqlalchemy.sql.select([func.json_agg(col)] if many
                              else [jcte])
    if not top_call:
        for whereclause in where.get(state.path, ()):
            q = q.where(whereclause)
        state.where = t_where
    return q


def make_pydantic_model(
    cfg: PgZeroJsonConfig,
    state: PgZeroJsonState = PgZeroJsonState(cte={}, do_pydantic=True, pydantic_models={}),
) -> Type[pydantic.BaseModel]:
    """Generate a Pydantic model matching JSON generated by select_json_sql()"""

    table_name = get_table_alias_name(cfg.table, cfg)
    assert table_name
    select_json_sql(cfg.table, state=state, cfg=cfg, skip_tables=cfg.skip_tables)
    return state.pydantic_models[table_name]


'''
def fetch_as_jsons(sql_query, params=dict(), stringify=False):
    """Execute SQL query with params returning a single JSON column.
       If stringify is True, don't parse results as Python dicts."""

    with transaction.atomic(), connection.cursor() as cur:
        cursor_name = f"get_sql_as_jsons_{os.getpid()}"
        cur.execute(f"DECLARE {cursor_name} CURSOR FOR {sql_query}", params)
        if stringify:
            from psycopg2.extras import register_default_json
            register_default_json(cur.cursor, loads=lambda x: x)
        while True:
            cur.execute(f"FETCH 2000 FROM {cursor_name}")
            chunk = cur.fetchall()
            if not chunk:
                break
            for data, in chunk:
                yield data
'''
