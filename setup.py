from setuptools import setup

setup(name='pgzerojson',
      version='0.2',
      description='generate PostgreSQL queries constructing nested JSON on the database side',
      url='http://github.com/tuffnatty/pgzerojson',
      author='Phil Krylov',
      author_email='phil.krylov@gmail.com',
      license='MIT',
      packages=['pgzerojson'],
      package_data={'pgzerojson': ['py.typed']},
      python_requires='>=3.6',
      install_requires=[
          'SQLAlchemy>=1.3.17',
          'pydantic>=1.5.1',
      ],
      zip_safe=False)
